<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class AdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
 
        $adminUser = User::factory()->create([
            'email' => 'admin@gmail.com',
            'password' => Hash::make('1234'),
            'role' => 'Administrator',
        ]);
        $adminUser->assignRole('Administrator');

        $clienteUser = User::factory()->create([
            'email' => 'client@gmail.com',
            'password' => Hash::make('1234'),
            'role' => 'Administrator',
        ]);
        $clienteUser->assignRole('Cliente');

        $superUser = User::factory()->create([
            'email' => 'super@gmail.com',
            'password' => Hash::make('1234'),
            'role' => 'Supervisor',
        ]);
        $superUser->assignRole('Supervisor');
    }
}
