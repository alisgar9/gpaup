<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
        public function run(): void
        {
            $role_admin = Role::create(['name' => 'Administrator']);
            $role_user = Role::create(['name' => 'user']);
            $role_cliente = Role::create(['name' => 'Cliente']);
            $role_supervisor = Role::create(['name' => 'Supervisor']);
    
            $upermission_read = Permission::create(['name' => 'ver-usuarios']);
            $upermission_edit = Permission::create(['name' => 'crear-usuarios']);
            $upermission_write = Permission::create(['name' => 'editar-usuarios']);
            $upermission_delete = Permission::create(['name' => 'eliminar-usuarios']);
    
            $cpermission_read = Permission::create(['name' => 'ver-clientes']);
            $cpermission_edit = Permission::create(['name' => 'crear-clientes']);
            $cpermission_write = Permission::create(['name' => 'editar-clientes']);
            $cpermission_delete = Permission::create(['name' => 'eliminar-clientes']);
    
            $spermission_read = Permission::create(['name' => 'ver-supervisores']);
            $spermission_edit = Permission::create(['name' => 'crear-supervisores']);
            $spermission_write = Permission::create(['name' => 'editar-supervisores']);
            $spermission_delete = Permission::create(['name' => 'eliminar-supervisores']);
    
            $rpermission_read = Permission::create(['name' => 'ver-roles']);
            $rpermission_edit = Permission::create(['name' => 'crear-roles']);
            $rpermission_write = Permission::create(['name' => 'editar-roles']);
            $rpermission_delete = Permission::create(['name' => 'eliminar-roles']);
    
            $ppermission_read = Permission::create(['name' => 'ver-permisos']);
            $ppermission_edit = Permission::create(['name' => 'crear-permisos']);
            $ppermission_write = Permission::create(['name' => 'editar-permisos']);
            $ppermission_delete = Permission::create(['name' => 'eliminar-permisos']);
    
            $prpermission_read = Permission::create(['name' => 'ver-zonas']);
            $prpermission_edit = Permission::create(['name' => 'crear-zonas']);
            $prpermission_write = Permission::create(['name' => 'editar-zonas']);
            $prpermission_delete = Permission::create(['name' => 'eliminar-zonas']);
    
            $taskpermission_read = Permission::create(['name' => 'ver-parques']);
            $taskpermission_edit = Permission::create(['name' => 'crear-parques']);
            $taskpermission_write = Permission::create(['name' => 'editar-parques']);
            $taskpermission_delete = Permission::create(['name' => 'eliminar-parques']);
    
            $compap_read = Permission::create(['name' => 'ver-empresas']);
            $compap_edit = Permission::create(['name' => 'crear-empresas']);
            $compap_write = Permission::create(['name' => 'editar-empresas']);
            $compap_delete = Permission::create(['name' => 'eliminar-empresas']);
            
            $devp_read = Permission::create(['name' => 'ver-dispositivos']);
            $devp_edit = Permission::create(['name' => 'crear-dispositivos']);
            $devp_write = Permission::create(['name' => 'editar-dispositivos']);
            $devp_delete = Permission::create(['name' => 'eliminar-dispositivos']);
    
            $permissions_admin = [
                $upermission_read, $upermission_edit, $upermission_write, $upermission_delete,
                $cpermission_read, $cpermission_edit, $cpermission_write, $cpermission_delete,
                $spermission_read, $spermission_edit, $spermission_write, $spermission_delete,
                $rpermission_read, $rpermission_edit, $rpermission_write, $rpermission_delete,
                $ppermission_read, $ppermission_edit, $ppermission_write, $ppermission_delete,
                $prpermission_read, $prpermission_edit, $prpermission_write, $prpermission_delete,
                $taskpermission_read, $taskpermission_edit, $taskpermission_write, $taskpermission_delete,
                $compap_read, $compap_edit, $compap_write, $compap_delete,
                $devp_read, $devp_edit, $devp_write, $devp_delete
            ];
    
            $permissions_super = [
                $cpermission_read, $cpermission_edit, $cpermission_write, $cpermission_delete,
                $prpermission_read, $prpermission_edit, $prpermission_write, $prpermission_delete,
                $compap_read, $compap_edit, $compap_write, $compap_delete,
                $devp_read, $devp_edit, $devp_write, $devp_delete
            ];
    
            
    
            $role_admin->syncPermissions($permissions_admin);
            $role_supervisor->syncPermissions($permissions_super);
            $role_cliente->givePermissionTo($prpermission_read);
        }
}
