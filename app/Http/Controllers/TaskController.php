<?php

namespace App\Http\Controllers;

use App\Models\Task;
use App\Http\Requests\StoreTaskRequest;
use App\Http\Requests\UpdateTaskRequest;
use Auth;

class TaskController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $this->authorize('ver-usuarios');
        $tasks = Task::all();
 
        return view('tasks.index', compact('tasks'));
    }
 
    public function create()
    {
        $this->authorize('crear-usuarios');
        return view('tasks.create');
    }
 
    public function store(StoreTaskRequest $request)
    {
        $this->authorize('crear-usuarios');
        Task::create($request->validated());
 
        return redirect()->route('tasks.index');
    }
 
    public function edit(Task $task)
    {
        return view('tasks.edit', compact('task'));
    }
 
    public function update(UpdateTaskRequest $request, Task $task)
    {
        $task->update($request->validated());
 
        return redirect()->route('tasks.index');
    }
 
    public function destroy(Task $task)
    {
        $task->delete();
 
        return redirect()->route('tasks.index');
    }
}