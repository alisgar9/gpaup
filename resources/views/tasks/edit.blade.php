<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Tasks edit') }}
        </h2>
    </x-slot>
 
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
            <form method="POST" action="{{ route('tasks.update', $task) }}">
                        @csrf
                        @method('PUT')
 
                        <div>
                            <x-label for="name" value="{{ __('name') }}" />
                            <x-input id="name" type="text" class="mt-1 block w-full" :value="$task->name" required autofocus autocomplete="name" />
                            <x-input-error for="name" class="mt-2" />
                        </div>
 
                        <div class="flex mt-4">
                            <x-button>
                                {{ __('Save Task') }}
                            </x-button>
                        </div>
            </form>
        </div>
    </div>
</x-app-layout>
